import css from './AssetsBar.module.css';
import { useState } from 'react'
import Arrow from '../../Assets/images/arrow_assets-bar.svg'


const AssetsBar = () => {
    const [isOpen, setIsOpen] = useState(false)

    const handleClick = (e) => {
        setIsOpen(!isOpen)

    }

    const onDragStart = (event, nodeType) => {
        event.dataTransfer.setData('application/reactflow', nodeType);
        event.dataTransfer.effectAllowed = 'move';
    };


    return (
        <aside>
            <button className={css.accordion} type={'button'} onClick={handleClick}>
                <img src={Arrow} className={isOpen ? `${css.arrowUp}` : null } alt="" width={24} height={24} />
                <h2 className={css.assetsHeader}>
                    Section 1
                </h2>
            </button>
            <div className={isOpen ? `${css.contentWrapper} ${css.isOpen}` : css.contentWrapper}>
                <div className={css.blocks}>
                    <div className={css.blockStart} onDragStart={(event) => onDragStart(event, 'startFinish')} draggable>
                        <h3 className={css.blockHeader}>
                            Start/Finish
                        </h3>
                        <div className={css.iconInfo} title="information">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                                <path d="M7.33301 4.66671H8.66634V6.00004H7.33301V4.66671ZM7.33301 7.33337H8.66634V11.3334H7.33301V7.33337ZM7.99967 1.33337C4.31967 1.33337 1.33301 4.32004 1.33301 8.00004C1.33301 11.68 4.31967 14.6667 7.99967 14.6667C11.6797 14.6667 14.6663 11.68 14.6663 8.00004C14.6663 4.32004 11.6797 1.33337 7.99967 1.33337ZM7.99967 13.3334C5.05967 13.3334 2.66634 10.94 2.66634 8.00004C2.66634 5.06004 5.05967 2.66671 7.99967 2.66671C10.9397 2.66671 13.333 5.06004 13.333 8.00004C13.333 10.94 10.9397 13.3334 7.99967 13.3334Z" fill="#625B71"/>
                            </svg>
                        </div>
                    </div>
                    <div className={css.blockAction} onDragStart={(event) => onDragStart(event, 'client')} draggable>
                        <div className={css.blockHeader}>
                            <h3>Action</h3>
                        <div className={css.iconInfo} title="information">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                                <path d="M7.33301 4.66671H8.66634V6.00004H7.33301V4.66671ZM7.33301 7.33337H8.66634V11.3334H7.33301V7.33337ZM7.99967 1.33337C4.31967 1.33337 1.33301 4.32004 1.33301 8.00004C1.33301 11.68 4.31967 14.6667 7.99967 14.6667C11.6797 14.6667 14.6663 11.68 14.6663 8.00004C14.6663 4.32004 11.6797 1.33337 7.99967 1.33337ZM7.99967 13.3334C5.05967 13.3334 2.66634 10.94 2.66634 8.00004C2.66634 5.06004 5.05967 2.66671 7.99967 2.66671C10.9397 2.66671 13.333 5.06004 13.333 8.00004C13.333 10.94 10.9397 13.3334 7.99967 13.3334Z" fill="#625B71"/>
                            </svg>
                        </div>
                        </div>
                        <p className={css.blockContent}>
                            Description
                        </p>
                        <p className={css.blockContentSmall}>
                            Sample requests
                        </p>
                    </div>
                    <div className={css.blockVarNotOk} onDragStart={(event) => onDragStart(event, 'operatorNotAllowed')} draggable>
                        <div className={css.blockHeader}>
                            {/* <h3>Variant not ok</h3> */}
                            <div>Variant not ok</div>
                        </div>
                        <div className={css.iconInfo} title="information">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                                <path d="M7.33301 4.66671H8.66634V6.00004H7.33301V4.66671ZM7.33301 7.33337H8.66634V11.3334H7.33301V7.33337ZM7.99967 1.33337C4.31967 1.33337 1.33301 4.32004 1.33301 8.00004C1.33301 11.68 4.31967 14.6667 7.99967 14.6667C11.6797 14.6667 14.6663 11.68 14.6663 8.00004C14.6663 4.32004 11.6797 1.33337 7.99967 1.33337ZM7.99967 13.3334C5.05967 13.3334 2.66634 10.94 2.66634 8.00004C2.66634 5.06004 5.05967 2.66671 7.99967 2.66671C10.9397 2.66671 13.333 5.06004 13.333 8.00004C13.333 10.94 10.9397 13.3334 7.99967 13.3334Z" fill="#625B71"/>
                            </svg>
                        </div>
                    </div>
                    <div className={css.blockVarOk} onDragStart={(event) => onDragStart(event, 'operatorAllowed')} draggable>
                        <div className={css.blockHeader}>
                            <div >
                                <h3>Variant ok</h3>
                            </div>
                            <div className={css.iconInfo} title="information">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                                    <path d="M7.33301 4.66671H8.66634V6.00004H7.33301V4.66671ZM7.33301 7.33337H8.66634V11.3334H7.33301V7.33337ZM7.99967 1.33337C4.31967 1.33337 1.33301 4.32004 1.33301 8.00004C1.33301 11.68 4.31967 14.6667 7.99967 14.6667C11.6797 14.6667 14.6663 11.68 14.6663 8.00004C14.6663 4.32004 11.6797 1.33337 7.99967 1.33337ZM7.99967 13.3334C5.05967 13.3334 2.66634 10.94 2.66634 8.00004C2.66634 5.06004 5.05967 2.66671 7.99967 2.66671C10.9397 2.66671 13.333 5.06004 13.333 8.00004C13.333 10.94 10.9397 13.3334 7.99967 13.3334Z" fill="#625B71"/>
                                </svg>
                            </div>
                        </div>

                        <p className={css.blockContent}>
                            Variant ok
                        </p>
                    </div>
                    <div className={css.blockDesicion} onDragStart={(event) => onDragStart(event, 'operatorNotAllowed')} draggable>
                        <div className={css.blockHeader}>
                            <h3>Desicion</h3>
                        </div>
                        <div className={css.iconInfo} title="information">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                                <path d="M7.33301 4.66671H8.66634V6.00004H7.33301V4.66671ZM7.33301 7.33337H8.66634V11.3334H7.33301V7.33337ZM7.99967 1.33337C4.31967 1.33337 1.33301 4.32004 1.33301 8.00004C1.33301 11.68 4.31967 14.6667 7.99967 14.6667C11.6797 14.6667 14.6663 11.68 14.6663 8.00004C14.6663 4.32004 11.6797 1.33337 7.99967 1.33337ZM7.99967 13.3334C5.05967 13.3334 2.66634 10.94 2.66634 8.00004C2.66634 5.06004 5.05967 2.66671 7.99967 2.66671C10.9397 2.66671 13.333 5.06004 13.333 8.00004C13.333 10.94 10.9397 13.3334 7.99967 13.3334Z" fill="#625B71"/>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
    )
}

export default AssetsBar;