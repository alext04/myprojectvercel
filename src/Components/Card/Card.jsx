import { useDispatch } from "react-redux";
import image from "../../Assets/images/image.png";

import css from "./Card.module.css";
import { scriptId } from "../../store/slices/scriptIdSlice";
import { useNavigate } from "react-router-dom";

function Card({ title, description, dateLastChange, idScript, itemClassName, last_modified_at}) {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const doubleClick = (e) => {
    dispatch(scriptId(idScript));
    navigate("/dialog");
  };
  const handleClick = () => {
    dispatch(scriptId(idScript));
  }

  return (
    <div 
    className={itemClassName === 'active' ? `${css.card} ${css.active}` : `${css.card}`} 
    onDoubleClick={doubleClick}
     onClick={handleClick}
     id={idScript}>
      <div className={css.image}>
        <img src={image} alt="img-card" className={css.img} />
      </div>
      <h2 className={css.title}>{title}</h2>
      <p className={css.paragraph}>{description}</p>
      <span className={css.date}>
        {/* {new Date(dateLastChange).toLocaleString()} */}
        {new Date(last_modified_at).toLocaleString()}
      </span>
    </div>
  );
}

export default Card;
