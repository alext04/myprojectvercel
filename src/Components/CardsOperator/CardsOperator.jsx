import { useState } from 'react';

import Card from '../Card/Card';
import css from './CardsOperator.module.css';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';

function CardsOperator({scriptsArray}) {

    const scriptId = useSelector(state => state.scriptId)
    const [idSelectedScript, setIdSelectedScript] = useState(scriptId);
    
    useEffect(()=>{
        setIdSelectedScript(scriptId)
    },[scriptId])

    return ( 
        <div className={css.wrapper}>
            {
                scriptsArray.map((item,index) => {
                    return(
                        <Card key={index} title={item.name} description={item.description} dateLastChange={item.time} 
                        last_modified_at={item.last_modified_at}
                        // idScript={item.id} 
                        idScript={item.uuid} 
                        // itemClassName={item.id === idSelectedScript ? 'active' : null}/>
                        itemClassName={item.uuid === idSelectedScript ? 'active' : null}/>
                    )
                })
            }
        </div>
     );
}

export default CardsOperator;