import css from "./LoginForm.module.css"
import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from 'react-redux'
import { logIn } from '../../../store/slices/userSlice'
import useAxios from '../../../hooks/useAxios'
import Cookies from "js-cookie";
import iconCross from "../../../Assets/images/icon-cross.svg";
import iconAttention from "../../../Assets/images/icon-attention.svg"


const LoginForm = () => {
    // const [error, setError] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const [url, setUrl] = useState(null)
    const [formData, setFormData] = useState(null)
    const [user, setUser] = useState(null)
    const dispatch = useDispatch()
    const [errorInputLoginLength, setErrorInputLoginLengh] =useState(false)
    const [errorInputPasswordLength, setErrorInputPasswordLengh] =useState(false)

    /* http://ec2-13-49-16-252.eu-north-1.compute.amazonaws.com:5000/swagger-ui/index.html */

    /* 
        useAxios params:
        1. isLoading - if not set to "true", then no request to API will be made
        2. url - API endpoint
        3. method (POST, GET etc)
        4. token (if needed)
        5. formData - request body
    */
    const { response, error } = useAxios(isLoading, url, 'POST', null, formData)

    useEffect(() => {
        setIsLoading(false)
        const data = response ? response.data : null
        console.log(data)

        if (data?.accessToken && data?.refreshToken) {
            Cookies.set("authLevel", 1);
            Cookies.set("userName", user);
            Cookies.set("accessToken", response.data.accessToken);
            Cookies.set("refreshToken", response.data.refreshToken);

            const auth = {
                authLevel: 1,
                userName: user,
                accessToken: response.data.accessToken,
                refreshToken: response.data.refreshToken,
            }

            dispatch(logIn(auth))
        }

        if (error) console.error("Произошла ошибка при входе:", error);
    }, [response, error, user, dispatch])

    const {
        register,
        reset,
        formState: {
            errors,
            isValid,
        },
        handleSubmit,
    } = useForm()

    const onSubmit = async (data) => {
        setUser(data.login)
        setFormData(data)
        setIsLoading(true)
        setUrl('auth/login')
    }

    return (
        <>
            <form action="" className={css.form} onSubmit={handleSubmit(onSubmit)}>
                <label className={error || errors?.login || errorInputLoginLength ? css.label_error : css.label}>
                    <input
                        name="login"
                        type="text"
                        className={css.input}
                        maxLength={60}
                        {...register("login", {
                            required: "Поле обязательно к заполнению",
                            minLength: {
                                value: 6,
                                message: "Минимальное количество символов 6",
                            },
                            onChange: (e) => {e.target.value.length >= 60 ? setErrorInputLoginLengh(true) : setErrorInputLoginLengh(false)},
                            onBlur: (e) => {e.target.value.length <= 60 && setErrorInputLoginLengh(false)},
                        })}
                    />
                    <img
                        className={css.img}
                        src={error ? iconAttention : iconCross}
                        alt={error ? "icon-attention" : "icon-cross"}
                        onClick={() => {
                            !error &&
                                reset(
                                    {
                                        login: ""
                                    },
                                    {
                                        keepErrors: true,
                                        keepDirty: true,
                                    }
                                )
                        }}
                    />
                    <span className={css.span}>Имя пользователя</span>
                </label>
                <div className={css.texterror}>
                    {
                        error ?
                            "Пользователь не найден или введён неправильный пароль" :
                            errors?.login ? errors.login.message : errorInputLoginLength ? "Максимальное количество символов 60" : "Введите имя пользователя"
                    }
                </div>
                <label className={error || errors?.password || errorInputPasswordLength ? css.label_error : css.label}>
                    <input
                        name="password"
                        type="password"
                        className={css.input}
                        maxLength={30}
                        {...register("password", {
                            required: "Поле обязательно к заполнению",
                            minLength: {
                                value: 8,
                                message: "Минимальное количество символов 8",
                            },
                            onChange: (e) => {e.target.value.length >= 30 ? setErrorInputPasswordLengh(true) : setErrorInputPasswordLengh(false)},
                            onBlur: (e) => {e.target.value.length <= 30 && setErrorInputPasswordLengh(false)},
                        })}
                    />
                    <img
                        className={css.img}
                        src={error ? iconAttention : iconCross}
                        alt={error ? "icon-attention" : "icon-cross"}
                        onClick={() => {
                            !error &&
                                reset(
                                    {
                                        password: ""
                                    },
                                    {
                                        keepErrors: true,
                                        keepDirty: true,
                                    }
                                )
                        }}
                    />
                    <span className={css.span}>Пароль</span>
                </label>
                <div className={css.texterror}>
                    {
                        error ?
                            "Пользователь не найден или введён неправильный пароль" :
                            errors?.password ? errors.password.message : errorInputPasswordLength ? "Максимальное количество символов 30" : "Введите пароль"
                    }
                </div>
                <div className={css.linkpassword}>Забыли пароль?</div>
                {/* <input type="submit" className={css.input} disabled={!isValid} value="Войти" /> */}
                <input type="submit" className={css.input} value="Войти" />
            </form>
        </>
    );
}

export default LoginForm;