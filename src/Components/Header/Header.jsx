import { useEffect, useState } from "react";
import React from "react";
import css from "./Header.module.css";
import { NavLink, Link, useLocation, useNavigate } from "react-router-dom";
import { shallowEqual, useSelector } from "react-redux";
import Notifications from "../Notifications";
import { Button } from "../Button/Button";
import Avatar from "../../Assets/images/header-avatar.svg";
import Bell from "../../Assets/images/header-bell.svg";
import close from "../../Assets/images/close.svg";
import logo from "../../Assets/images/logo.png"
import LinkButton from "../LinkButton/LinkButton";


const Header = () => {
  const [isOpen, setIsOpen] = useState(false);
  const navigate = useNavigate()
  const { pathname } = useLocation();
  const { userName } = useSelector((state) => state.user, shallowEqual);
  // const [notifyActive, setNotifyActive] = useState('')

  // for conditional rendering of Help link
  const hasHelp = [
    "/operator",
    "/coordinator",
    "/administrator",
    "/constructor",
    "/administrator/templates",
    "/administrator/allscripts",
    "/administrator/drafts",
    "/administrator/comments",
  ];


  // const handleClick = () => setIsOpen(!isOpen);
  const handleClick = () => {
    setIsOpen(false);
    navigate("/administrator/comments")
  }
  const handleMouseOver = () => setIsOpen(true);
 // const handleMouseLeave = () => setIsOpen(false);

  /* const handleClick = () => setIsOpen(!isOpen); */


  const getTitle = () => {
    switch (pathname) {
      case "/operator":
        return "Оператор";
      case "/dialog":
        return "Диалог";
      case "/coordinator":
        return "Координатор";
      case "/help":
        return "Центр помощи";
      case "/administrator":
        return "Администратор";
      case "/mainadministrator":
        return "Главный администратор";
      case "/constructor":
        return "Конструктор скриптов";
      case "/administrator/templates":
        return "Шаблоны";
      case "/administrator/allscripts":
        return "Все скрипты";
      case "/administrator/drafts":
        return "Черновики";
      case "/administrator/comments":
        return "Комментарии от оператора";
      default:
        return null;
    }
  };
  // };


  const handleMouseEnter = () => {
    /* console.log("наведение"); */
    setIsOpen(!isOpen)
  };

  const handleMouseLeave = () => {
/*     console.log("уведение"); */
    setIsOpen(!isOpen)
    setTimeout(() => {
        setIsOpen(!isOpen)
      }, 2000);
  };
  const existingComments = JSON.parse(sessionStorage.getItem('comments'));
  const [counterBell, setCounterBell] = useState(0);
  useEffect(()=>{
    if(existingComments){
      setCounterBell((JSON.parse(sessionStorage.getItem('comments')).length))
    } else {
      setCounterBell(0)
    }
    console.log(existingComments);
  },[existingComments])
  const getHeaderRight = () => {
    if (!userName || pathname === "/" || pathname === "/operator" || pathname === "/dialog") {
      return (
        <div className={css.help}>
          <Link to="/help">Центр помощи</Link>
        </div>
      );
    } else {
      return (
        <div
          className={
            hasHelp.includes(pathname)
              ? css.headerRight
              : `${css.headerRight} ${css.headerRightNoHelp}`
          }
        >
          <div className={css.avatar}>
            <img className={css.icon} src={Avatar} alt="аватар" />
          </div>
          <div className={css.welcome}>Привет, {userName}</div>
          {pathname === "/administrator/comments" ? null : <div
            className={
              hasHelp.includes(pathname) ? css.notify : css.notifyNoHelp
            }
            // onClick={handleClick}
            onMouseOver={handleMouseOver}
            onMouseLeave={handleMouseLeave}
          >
            <img
              className={css.icon}
              src={Bell}
              alt="колокольчик"
              width="24"
              height="24"
            />
            <div className={css.counter}>
              <span>{counterBell}</span>
            </div>
            {isOpen ? (
              <div className={css.dropdown}>
                <div className={css.dropdownTop}>
                  <h2 className={css.headerNotifyTitle}>Уведомления</h2>
                  <Button variant={'notifyModal'} handleClick={handleClick}>Больше</Button>
                  {/* <Button
                    type={"button"}
                    variant={"close"}
                    handleClick={handleClick}
                  >
                    <img
                      src={close}
                      alt="закрыть окно"
                      className={css.cross}
                      width={24}
                      height={24}
                    />
                  </Button> */}
                </div>
                <Notifications listClass={"headerList"} closeNotity={handleClick}/>
              </div>
            ) : null}
          </div>}
          {hasHelp.includes(pathname) && (
            <div className={css.help}>
              <Link to="/help">Центр помощи</Link>
            </div>
          )}
        </div>
      );
    }
  };
  return (
    <header className={css.header}>
      <div className={css.container}>
        <div className={css.headerLeft}>
          <div to="/" className={css.logo}>
            <div className={css.logo_img}></div>
            <div className={css.logo_name}>script <br/> builder</div>
          </div>
          <h1 className={css.headerTitle}>{getTitle()}</h1>
        </div>
        {getHeaderRight()}
      </div>
    </header>
  );
};

export default Header;
