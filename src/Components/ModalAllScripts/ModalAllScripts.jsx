import css from "./ModalAllScripts.module.css";
import { Button } from "../Button/Button";

function ModalAllScripts({closeModalAllScriptsDelete, closeModalAllScriptsCancel}) {

    const handleClickDelete = () => {
        // axios.delete(`https://api.doscript.pnpl.tech/api/v2/script/del-script/${idScript}`)
        closeModalAllScriptsDelete(false)
    }
    const handleClickCancel = () => {
        closeModalAllScriptsCancel(false)
    }
    return ( 
        <div className={css.wrapper}>
            <div className={css.content}>
                <h2 className={css.title}>Вы хотите удалить отмеченные скрипты?</h2>
                <div className={css.button_wrapper}>
                <Button variant={'modalAllScriptsDelete'} handleClick={handleClickDelete}>Удалить</Button>
                <Button variant={'modalAllScriptsCancel'} handleClick={handleClickCancel}>Отменить</Button>
            </div>
            </div>
        </div>
     );
}

export default ModalAllScripts;