import { useSelector } from "react-redux";
import { Button } from "../Button/Button";
import css from "./ModalScriptConstructor.module.css";
import axios from "axios";
import { useEffect, useState } from "react";

function ModalScriptConstructor({openModal}) {
  
    const states = useSelector(state => state.tree)
    async function saveNewScript(jsonData) {
            await axios.post('https://api.doscript.pnpl.tech/api/v2/script/new-script', 
          jsonData,
           { headers: {
            'Content-Type': 'application/json',
            }}
        )
          .then((response) => {
            console.log(response.data);
          })
          .catch((error) => {
            console.error(error);
          });
    }
    const handleClickCancel = () => {
        openModal(false)
    }
    const handleClickSave = () => {
        const jsonData = JSON.stringify(states)
        console.log(jsonData);
        saveNewScript(jsonData)
        openModal(false)
    }
    return ( 
   <div className={css.wrapper}>
        <div className={css.content}>
            <div className={css.title}>Сохранить?</div>
            <label className={css.label}>
                <input type="text" className={css.input}/>
                <span className={css.input_title}>Название</span>
            </label>
            <div className={css.radioButtons_wrapper}>
                <label className={css.label_radio}>
                    <input type="radio" name="radio" className={css.input_radio}/>
                    <span>Как черновик</span>
                </label>
                <label className={css.label_radio}>
                    <input type="radio" name="radio" className={css.input_radio}/>
                    <span>Как шаблон</span>
                </label>
            </div>
            <div className={css.button_wrapper}>
                <Button variant={'modalConstructorCancel'} handleClick={handleClickCancel}>Отмена</Button>
                <Button variant={'modalConstructorSave'} handleClick={handleClickSave}>Сохранить</Button>
            </div>
        </div>
    </div>
    );
}

export default ModalScriptConstructor;