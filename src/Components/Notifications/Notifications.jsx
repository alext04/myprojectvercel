import css from './Notifications.module.css';
import notificationIcon from '../../Assets/images/notification-icon.png'
import { useNavigate } from 'react-router-dom';


const Notifications = ({ listClass, closeNotity }) => {
    const navigate = useNavigate()
    const notifications = [
        {
            id: `1${listClass}`,
            img: notificationIcon,
            title: '%author’s name%',
            text: 'Lorem ipsum dolor sit amet consectetur. Dictumst nunc urna nisl ut vulputate et mauris urna et. Proin quis diam lobortis sodales.',
            date: '1 Jan 2023',
            time: '15:32'
        },
        {
            id: `2${listClass}`,
            img: notificationIcon,
            title: '%author’s name%',
            text: 'Lorem ipsum dolor sit amet consectetur. Dictumst nunc urna nisl ut vulputate et mauris urna et. Proin quis diam lobortis sodales.',
            date: '1 Jan 2023',
            time: '15:32'
        }
    ]

    const cls = {
        adminList: `${css.adminList}`,
        headerList: `${css.headerList}`
    }

    const existingComments = JSON.parse(sessionStorage.getItem('comments')) || [];
    const handleClick = () => {
        navigate('administrator/comments')
        closeNotity()
        console.log(existingComments);
    }


    return (
        <ul className={cls[`${listClass}`]}>
            {
    existingComments.map((item, index) => {
      const mergedItem = {
        id: `2${index}`,
        img: notificationIcon,
        title: '%author’s name%',
        ...item
      };

      return (
        <li className={css.notification} key={mergedItem.id} onClick={handleClick}>
          <div className={css.notificationTop}>
            <img src={mergedItem.img} alt="" width={76} height={76} />
            <div className={css.notificationText}>
              <h3>{mergedItem.title}</h3>
              <p>{mergedItem.text}</p>
            </div>
          </div>
          <div className={css.notificationBottom}>
            <p>{mergedItem.date}</p>
            <p>{mergedItem.time}</p>
          </div>
        </li>
      );
    })
  }
        </ul>

    )

}

export default Notifications;
