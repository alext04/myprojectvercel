import { useSelector } from "react-redux";
import Table from "../../Components/Table/Table";
import css from "./OperatorWindow.module.css";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const OperatorWindow = () => {
  const array = useSelector((state) => state.tree.nodes);

  // if(array.length === 0) return;
  const idRootPhrase = array.filter((item) => item.isRoot === true)
  const [operatorPhrase, setOperatorPhrase] = useState([]);
  const [clientPhrase, setClientPhrase] = useState([]);
  const phrase = useSelector((state) => state.scriptId);
  // const [dialog, setDialog] = useState(phrase);
  const [nextIdPhrase, setNextIdPhrase] = useState();
  // const [idPhrase, setIdPhrase] = useState()
  const [arr, setArr] = useState([]);
  const [activeModal, setActiveModal] = useState(false);
  const [titleBlock, setTitleBlock] = useState();
  const [comments, setComments] = useState("info");
  const [requestBlock, setRequestBlock] = useState("");
  const [explanationPhrase, setExplanationPhrase] = useState("");
  const [flag, setFlag] = useState(false);

  async function getPhrase(id) {
    if(id.length === 0) return
    setOperatorPhrase(id);
    id.forEach((item) => setOperatorPhrase([...operatorPhrase, item]));
    setNextIdPhrase(id[0].childrenIds);
  }
  useEffect(() => {
    getPhrase(idRootPhrase);
  }, []);

  function getPhraseNext(id) {
    if (id === "") {
      return;
    }
    const arrayNextId = id.split(",");
    let newArrayPhrases = arrayNextId
      .map((itemId) => array.find((item) => item.uuid === itemId))
      .sort((a, b) => (a.type < b.type ? -1 : 1));
    newArrayPhrases[0] !== undefined &&
    newArrayPhrases[0].isOperatorPhrase === false
      ? setClientPhrase([...newArrayPhrases])
      : setOperatorPhrase([...newArrayPhrases]);
  }

  const closeModal = (data) => {
    setActiveModal(data);
  };

  const Modal = ({
    setActiveModal,
    titleBlock,
    openComments,
    requestBlock,
    explanationPhrase,
  }) => {
    const [comments, setComments] = useState(openComments);
    const navigate = useNavigate();
    const modal = {
      position: "fixed",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: "100%",
      height: "100%",
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      background: "rgba(0, 0, 0, 0.5)",
      zIndex: 20,
    };
    const wrapper = {
      display: "flex",
      width: "694px",
      padding: "64px",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      gap: "32px",
      borderRadius: "10px",
      background: "white",
    };
    const title = {
      marginBottom: "10px",
      fontFamily: "Lato",
      fontSize: "16px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "24px",
    };
    const subtitle = {
      marginBottom: "30px",
      fontFamily: "Lato",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: "20px",
    };
    const buttonWrapper = {
      width: "100%",
      display: "flex",
      justifyContent: "flex-end",
    };
    const buttonLeft = {
      marginLeft: "20px",
      padding: "10px 20px",
      borderRadius: "10px",
      fontFamily: "Roboto",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "20px",
      color: "#156075",
      background: "transparent",
    };
    const buttonRight = {
      marginLeft: "20px",
      padding: "10px 24px",
      borderRadius: "30px",
      fontFamily: "Roboto",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "20px",
      color: "#ffffff",
      background: "#156075",
    };
    const titleWrapper = {
      padding: "24px",
      border: "2px solid #4AB9F5",
      borderRadius: "10px",
      marginBottom: "16px",
    };
    const modalContent = {
      width: "100%",
      height: "100%",
    };
    const contentText = {
      fontFamily: "Lato",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: "20px",
      overflowY: "scroll",
    };
    const textareaItem = {
      width: "100%",
      padding: "16px",
      borderRadius: "4px",
      border: "1px solid #D5D5DE",
    };
    const dialog = {
      marginLeft: "20px",
      padding: "10px 20px",
      fontFamily: "Roboto",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "20px",
      color: "#156075",
      background: "transparent",
      border: "1px solid #156075",
      borderRadius: "100px",
    };
    const handleClick = () => {
      setActiveModal(false);
      // setComments(true)
      setComments("info");
      document.querySelector("body").classList.remove("is-lock");
    };
    const handleClickComments = () => {
      // setComments(false)
      setComments("comment");
    };
    const handleClickFinish = () => {
      setActiveModal(false);
      navigate("/");
    };
    const showModalContent = (content) => {
      switch (content) {
        case "info":
          return (
            <div className="modal_wrapper" style={wrapper}>
              <div className="modal_content" style={modalContent}>
                <h2 className="content_title" style={title}>
                  {titleBlock}
                </h2>
                <h3 className="content_subtitle" style={subtitle}>
                  {requestBlock}
                </h3>
                <div className="content_text" style={contentText}>
                  {explanationPhrase}
                </div>
              </div>
              <div className="modal_buttons" style={buttonWrapper}>
                <button
                  className="button_comments"
                  style={buttonLeft}
                  onClick={handleClickComments}
                >
                  Оставить комментарий
                </button>
                <button
                  className="button_exite"
                  style={buttonRight}
                  onClick={handleClick}
                >
                  OK
                </button>
              </div>
            </div>
          );
        case "comment":
          return (
            <div className="modal_wrapper" style={wrapper}>
              <div className="modal_content" style={modalContent}>
                <div className="title_wrapper" style={titleWrapper}>
                  <h2 className="content_title" style={title}>
                    {titleBlock}
                  </h2>
                  <h3 className="content_subtitle" style={subtitle}>
                    {requestBlock}
                  </h3>
                </div>
                <textarea
                  className="content_textarea"
                  name=""
                  id=""
                  cols="30"
                  rows="10"
                  placeholder="%текст_комментария%"
                  style={textareaItem}
                ></textarea>
              </div>
              <div className="modal_buttons" style={buttonWrapper}>
                <button
                  className="button_comments"
                  style={buttonLeft}
                  onClick={handleClick}
                >
                  Отмена
                </button>
                <button
                  className="button_exite"
                  style={buttonRight}
                  onClick={handleClick}
                >
                  Отправить
                </button>
              </div>
            </div>
          );
        case "finish":
          return (
            <div className="modal_wrapper" style={wrapper}>
              <div className="modal_content" style={modalContent}>
                <div className="content_text">
                  Вы уверены, что хотите завершить диалог?
                </div>
              </div>
              <div className="modal_buttons" style={buttonWrapper}>
                <button
                  className="button_comments dialog"
                  style={dialog}
                  onClick={handleClickFinish}
                >
                  Да
                </button>
                <button
                  className="button_exite"
                  style={buttonRight}
                  onClick={handleClick}
                >
                  Отмена
                </button>
              </div>
            </div>
          );

        default:
      }
    };
    return (
      <div className="modal" style={modal}>
        {showModalContent(comments)}
      </div>
    );
  };

  const handleClick = (e) => {
    e.currentTarget.dataset.blockName !== "Диалог" &&
      getPhraseNext(e.currentTarget.dataset.nextid);
    const idItem = e.currentTarget.id;
    const mergeArrays = [...clientPhrase, ...operatorPhrase];
    const clickedElement = [...mergeArrays].find(
      (item) => item.uuid === idItem
    );
    let item = arr.some((item) => item.uuid === idItem);
    if (clickedElement.type === "operatorNotAllowed" || item === true) return;
    setArr([...arr].some((item) => item.uuid === idItem) ? [...arr] : [...arr, clickedElement]);
    if (item || clickedElement.type === arr[arr.length - 1].type) {
      setArr([...arr.slice(0, arr.length - 1), clickedElement]);
    } else {
      setArr([...arr, clickedElement]);
    }
  };
  const handleClickInfo = (e) => {
    e.stopPropagation();
    document.querySelector("body").classList.add("is-lock");
    console.log(
      e.currentTarget.dataset.blockName,
      e.currentTarget.dataset.parentId
    );
    const titleBlock = e.currentTarget.dataset.blockName;
    array.forEach((item) => {
      if (item.uuid === e.currentTarget.dataset.parentId) {
        setRequestBlock(item.data.label);
        setExplanationPhrase(item.data.note);
      }
    });
    setTitleBlock(titleBlock);
    setActiveModal(true);
    // setComments(true)
    setComments("info");
  };
  const handleClickCommentsDialog = (e) => {
    e.stopPropagation();
    document.querySelector("body").classList.add("is-lock");
    const titleBlock = e.currentTarget.dataset.blockName;
    array.forEach((item) => {
      if (item.uuid === e.currentTarget.dataset.parentId) {
        setRequestBlock(item.data.label);
      }
    });
    setTitleBlock(titleBlock);
    setActiveModal(true);
    setComments("comment");
  };
  const handleClickFinish = () => {
    setActiveModal(true);
    setComments("finish");
  };

  return (
    <>
      <div className={css.wrapper}>
        <div className={css.container}>
          <div className={css.container_replica}>
            <div className={css.replica_client}>
              <h2 className={css.client_title}>Реплики клиента</h2>
              {
                clientPhrase
                  ? clientPhrase.map((item, index) => {
                      return (
                        <div
                          className={`${css.item_wrapper} ${css.client}`}
                          key={index}
                          onClick={handleClick}
                          // id={idPhrase}
                          id={item.uuid}
                          data-nextid={item.childrenIds}
                          data-block-name="Реплика клиента"
                        >
                          <div className={css.item_inner}>
                            <p className={css.item_phrase}>{item.data.label}</p>
                            <p className={css.item_request}>Запрос</p>
                          </div>
                          <div
                            className={css.item_image}
                            onClick={handleClickInfo}
                            data-block-name="Реплика клиента"
                            data-parent-id={item.uuid}
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              fill="none"
                              className={css.item_img}
                            >
                              <path
                                d="M7.33301 4.66683H8.66634V6.00016H7.33301V4.66683ZM7.33301 7.3335H8.66634V11.3335H7.33301V7.3335ZM7.99967 1.3335C4.31967 1.3335 1.33301 4.32016 1.33301 8.00016C1.33301 11.6802 4.31967 14.6668 7.99967 14.6668C11.6797 14.6668 14.6663 11.6802 14.6663 8.00016C14.6663 4.32016 11.6797 1.3335 7.99967 1.3335ZM7.99967 13.3335C5.05967 13.3335 2.66634 10.9402 2.66634 8.00016C2.66634 5.06016 5.05967 2.66683 7.99967 2.66683C10.9397 2.66683 13.333 5.06016 13.333 8.00016C13.333 10.9402 10.9397 13.3335 7.99967 13.3335Z"
                                fill="current"
                              />
                            </svg>
                          </div>
                        </div>
                      );
                      // }
                    })
                  : null
              }
              <div className={css.client_container}></div>
            </div>
            <div className={css.replica_operator}>
              <h2 className={css.operator_title}>Реплики оператора</h2>
              {
                operatorPhrase
                  ? operatorPhrase.map((item, index) => {
                      return (
                        <div
                          className={
                            item.type === "startFinish" ||
                            item.type === "operatorAllowed"
                              ? `${css.item_wrapper} ${css.operator}`
                              : `${css.item_wrapper} ${css.operator} ${css.notAllowed}`
                          }
                          onClick={handleClick}
                          key={index}
                          // id={idPhrase}
                          id={item.uuid}
                          data-nextid={item.childrenIds}
                          data-block-name="Реплика оператора"
                        >
                          <div className={css.item_inner}>
                            <p className={css.item_phrase}>{item.data.label}</p>
                            <p className={css.item_request}>Запрос</p>
                          </div>
                          <div
                            className={css.item_image}
                            onClick={handleClickInfo}
                            data-block-name="Реплика оператора"
                            data-parent-id={item.uuid}
                          >
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              viewBox="0 0 16 16"
                              fill="none"
                              className={css.item_img}
                            >
                              <path
                                d="M7.33301 4.66683H8.66634V6.00016H7.33301V4.66683ZM7.33301 7.3335H8.66634V11.3335H7.33301V7.3335ZM7.99967 1.3335C4.31967 1.3335 1.33301 4.32016 1.33301 8.00016C1.33301 11.6802 4.31967 14.6668 7.99967 14.6668C11.6797 14.6668 14.6663 11.6802 14.6663 8.00016C14.6663 4.32016 11.6797 1.3335 7.99967 1.3335ZM7.99967 13.3335C5.05967 13.3335 2.66634 10.9402 2.66634 8.00016C2.66634 5.06016 5.05967 2.66683 7.99967 2.66683C10.9397 2.66683 13.333 5.06016 13.333 8.00016C13.333 10.9402 10.9397 13.3335 7.99967 13.3335Z"
                                fill="current"
                              />
                            </svg>
                          </div>
                        </div>
                      );
                      // }
                    })
                  : null
              }
            </div>
          </div>
          <div className={css.container_dialog}>
            {arr.length > 0
              ? arr.map((item, index) => {
                  return (
                    <div
                      className={
                        item.type === "startFinish" ||
                        item.type === "operatorAllowed"
                          ? `${css.item_wrapper} ${css.dialog_operator}`
                          : `${css.item_wrapper} ${css.dialog_client}`
                      }
                      onClick={handleClick}
                      key={index}
                      // id={idPhrase}
                      // id={item.id}
                      id={item.uuid}
                      // data-nextid = {item.uuid}
                      data-block-name="Диалог"
                    >
                      <div className={css.item_inner}>
                        <p
                          className={`${css.item_request} ${css.request_dialog}`}
                        >
                          {item.isOperatorPhrase
                            ? "Выявление потребностей"
                            : "Запрос"}
                        </p>
                        <p
                          className={`${css.item_phrase} ${css.phrase_dialog}`}
                        >
                          {item.data.label}
                        </p>
                      </div>
                      <div className={css.item_images}>
                        <div
                          className={css.item_image}
                          onClick={handleClickInfo}
                          data-block-name="Диалог"
                          data-parent-id={item.uuid}
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                            fill="none"
                            className={css.item_img}
                          >
                            <path
                              d="M7.33301 4.66683H8.66634V6.00016H7.33301V4.66683ZM7.33301 7.3335H8.66634V11.3335H7.33301V7.3335ZM7.99967 1.3335C4.31967 1.3335 1.33301 4.32016 1.33301 8.00016C1.33301 11.6802 4.31967 14.6668 7.99967 14.6668C11.6797 14.6668 14.6663 11.6802 14.6663 8.00016C14.6663 4.32016 11.6797 1.3335 7.99967 1.3335ZM7.99967 13.3335C5.05967 13.3335 2.66634 10.9402 2.66634 8.00016C2.66634 5.06016 5.05967 2.66683 7.99967 2.66683C10.9397 2.66683 13.333 5.06016 13.333 8.00016C13.333 10.9402 10.9397 13.3335 7.99967 13.3335Z"
                              fill="current"
                            />
                          </svg>
                        </div>
                        <div
                          className={css.item_image_comment}
                          onClick={handleClickCommentsDialog}
                          data-block-name="Диалог"
                          data-parent-id={item.uuid}
                        >
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            viewBox="0 0 16 16"
                            fill="none"
                            className={css.item_img}
                          >
                            <path
                              d="M14.6597 2.66683C14.6597 1.9335 14.0663 1.3335 13.333 1.3335H2.66634C1.93301 1.3335 1.33301 1.9335 1.33301 2.66683V10.6668C1.33301 11.4002 1.93301 12.0002 2.66634 12.0002H11.9997L14.6663 14.6668L14.6597 2.66683ZM13.333 2.66683V11.4468L12.553 10.6668H2.66634V2.66683H13.333Z"
                              fill="current"
                            />
                          </svg>
                        </div>
                      </div>
                    </div>
                  );
                })
              : null}
          </div>
        </div>
        {activeModal ? (
          <Modal
            setActiveModal={closeModal}
            titleBlock={titleBlock}
            openComments={comments}
            requestBlock={requestBlock}
            explanationPhrase={explanationPhrase}
          />
        ) : null}
      </div>
    </>
  );
};

export default OperatorWindow;
