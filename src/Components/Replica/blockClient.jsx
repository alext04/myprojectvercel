import React from 'react';
import { useSelector } from 'react-redux';

const BlockClient = () => {
  const phrases = useSelector((state) => state.blocks.blocks[2]?.phrase || 'пусто');

  return (
    <>
      {phrases}
    </>
  );
};

export default BlockClient;
