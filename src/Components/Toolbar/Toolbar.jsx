import { useState,useRef,useEffect } from "react";
import css from "./Toolbar.module.css"
import { Button } from "../Button/Button";
import checkmark from "../../Assets/images/checkmark.svg";
import {updateScriptIdName, updateTree, addTreeNode} from "../../store/slices/treeSlice";
import { updateNodes } from "../../store/slices/nodesSlice";
import { updateEdges } from "../../store/slices/edgesSlice";
import { v4 as uuidv4 } from "uuid";
import axios from "axios";
import { useDispatch,useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const Toolbar = ({onClick, openModalConstructor}) => {
    const[btnactiveEdition, setBtnactiveEdition] = useState('editionActive')
    const [btnactiveViewing, setBtnactiveViewing] = useState('viewing')
    const [titleScript, setTitleScript] = useState("%название скрипта%")
    const [titleEdit, setTitleEdit] = useState(false)
    const [tooltip, setTooltip] = useState(false)

    const dispatch = useDispatch()
    const navigate = useNavigate()
    const state = useSelector(state => state.tree)
    const tree = useSelector(state => state.tree)
    const nodes = useSelector(state => state.nodes)
    const edges = useSelector(state => state.edges)
    const refTitleWrapper = useRef()

    const handleClick = (value) => {
        onClick(value)
    }
    const handleDoubleClick = () => {
        setTitleEdit(true)
    }
    const handleClickEdition = () => {
        setBtnactiveEdition('editionActive')
        setBtnactiveViewing('viewing')
        handleClick(true)
    }
    const handleClickViewing = () => {
        setBtnactiveEdition('edition')
        setBtnactiveViewing('viewingActive')
        handleClick(false)
     }
     const handleChange = (e) => {
        setTitleScript(e.target.value)
     }


    async function saveNewScript(jsonData) {
    //     await axios.post('https://api.doscript.pnpl.tech/api/v2/script/new-script', 
    //   jsonData,
    //    { headers: {
    //     'Content-Type': 'application/json',
    //     }}
    // )
    //   .then((response) => {
    //     console.log(response.data);
    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   });
    const jsonDatas = JSON.stringify(state)
      console.log(jsonData);
}

     const handleClickSave = () => {
        const id = uuidv4()
        dispatch(updateScriptIdName( {scriptId: id,scriptName: titleScript}))
        // const jsonData = JSON.stringify(state)
        // console.log(jsonData);
        // saveNewScript(jsonData)
        const script = {
            tree: tree,
            nodes: nodes,
            edges: edges
        }
        localStorage.setItem('script', JSON.stringify(script));
        openModalConstructor(true)
     }
     const handleClickBack = () => {
        navigate('/')
     }
     const fetchData = async () => {
        try {
          const response = await axios('https://api.doscript.pnpl.tech/api/v2/script/all/');
          response.data.forEach(item => {
            if(item.name === titleScript) {
                setTooltip(true)
                setTimeout(closeTooltip, 5000);
            }
        })
        } catch (error) {
          console.error('Error:', error);
        }
      };
      
     const closeTooltip = () => {
        setTooltip(false)
     }
     const handleClickCloseTitle = (e) => {
         if(refTitleWrapper.current && !refTitleWrapper.current.contains(e.target)) {
             console.log(titleScript,/^\s{1}/.test(titleScript));
             if(titleScript === "" || /^\s{1}/.test(titleScript)) {
                setTitleScript("%название скрипта%")
            }
            setTitleEdit(false);
            
            fetchData();
        }
    }
    const handleClickApprove = () => {
        const script = JSON.parse(localStorage.getItem('script'));console.log(script);
        script.nodes.forEach((item) => {
            dispatch(updateNodes(item))
        })
        script.edges.forEach((item) => {
            dispatch(updateEdges(item))
        })
        dispatch(updateTree(script.tree.nodes))
    }
     useEffect(() => {
        document.addEventListener("click", handleClickCloseTitle);
        return () => document.removeEventListener("click", handleClickCloseTitle);
      }, [titleScript]);

    return ( 
        <div className={css.wrapper}>
        <div className={css.container}>
            <div className={css.item_left}>
                <div  className={css.title_wrapper}>
                    {
                        titleEdit ? <input ref={refTitleWrapper} type="text" className={css.title_input} onChange={handleChange} placeholder="%название скрипта%" onClick={handleClickCloseTitle} /> : 
                        <h2 className={tooltip ? `${css.title} ${css.active}` : css.title} onDoubleClick={handleDoubleClick}>{titleScript}</h2>
                    }

                </div>
                <div className={css.segmentes_button_wrapper}>
                <Button variant={btnactiveEdition} handleClick={handleClickEdition} id="edition">
                    {
                        btnactiveEdition === 'editionActive' ? <img className={css.checkmark} src={checkmark} alt='checkmark'/> : null
                    }
                      Редактирование
                </Button>
                <Button variant={btnactiveViewing} handleClick={handleClickViewing} id="viewing">
                {
                        btnactiveViewing === 'viewingActive' ? <img className={css.checkmark} src={checkmark} alt='checkmark'/> : null
                    }
                      Просмотр
                </Button>
                </div>
            </div>
            <div className={css.item_right}>
            <Button variant={'back'} handleClick={handleClickBack}>Назад</Button>
            <Button variant={'save'} handleClick={handleClickSave}>Сохранить</Button>
            <Button variant={'approve'} handleClick={handleClickApprove}>Согласовать</Button>
            </div>
        </div>
        </div>
     );
}

export default Toolbar;