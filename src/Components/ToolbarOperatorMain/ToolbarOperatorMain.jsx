import ReactSelect from 'react-select'

import css from "./ToolbarOperatorMain.module.css";
import iconExit from "../../Assets/images/icon-exit.svg";
import iconSort from "../../Assets/images/icon-sort.svg";
import iconViewCard from "../../Assets/images/icon-view-card.svg";
import iconViewTable from "../../Assets/images/icon-view-table.svg";
import iconCross from "../../Assets/images/icon-cross-button.svg";
import search from "../../Assets/images/search.svg";
import basket from "../../Assets/images/icon-basket.svg";
import axios from 'axios';

import {styles} from "./selectConfig";
import LinkButton from '../LinkButton/LinkButton';
import { Button } from '../Button/Button';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useLocation } from 'react-router-dom';

function ToolbarOperatorMain({sortScripts,viewScripts,sortButtonTable,valueId,checked,turnSortButtonInit,turnSortButton, onSearch, openModalAllScripts}) {
    const [buttonView, setButtonView] = useState(true)
    const [buttonSort, setButtonSort] = useState(true);
    const [userChoice, setUserChoice] = useState("");

    const { pathname } = useLocation();

    const idScript = useSelector(state => state.scriptId)

    const options = [
        {value: 'Дата создания', label: 'Дата создания'},
        {value: 'Название', label: 'Название'},
        {value: 'Последнее изменение', label: 'Последнее изменение'}
    ]

    // useEffect(()=>{
    //     sortButtonTable && setButtonSort(true)
    // },[sortButtonTable])

    useEffect(() => {
        turnSortButton && setButtonSort(true)
    },[turnSortButton])

    const handleClickSearch = (e) => {
        e.preventDefault()

    }

    const handleClickSort = () => {
        // if(userChoice === '') {console.log(userChoice);
        //     sortScripts(userChoice,buttonSort)
        //     setButtonSort(!buttonSort)
        // } else if(userChoice === 'Дата создания') {
        //     setButtonSort(!buttonSort)
        //     console.log('Дата создания')
        //     sortScripts(userChoice,buttonSort)
        // } else if(userChoice === 'Название') {
        //     setButtonSort(!buttonSort)
        //     console.log('Название')
        //     sortScripts(userChoice,buttonSort)
        // } else if(userChoice === 'Последнее изменение') {
        //     setButtonSort(!buttonSort)
        //     console.log('Последнее изменение')
        //     sortScripts(userChoice,buttonSort)
        // }
        if(userChoice === "" || userChoice) {
            setButtonSort(!buttonSort)
            sortScripts(userChoice,buttonSort)
        }
        turnSortButtonInit(false)
    }
    const handleChange = (choice) => {
        setUserChoice(choice.value)
        setButtonSort(true);
    }
    const handleClickView = () => {
        setButtonView(!buttonView)
        viewScripts(buttonView)
        console.log('view');
    }
    const [scripts,setScripts] = useState([])
    useEffect(() => {
        const fetchData = async () => {
            try {
        let numberScript = 1;
        const response = await axios('https://api.doscript.pnpl.tech/api/v2/script/all/');
        setScripts([...response.data].map((item)=>{
                        return {...item, number: numberScript++}
                    }).reverse())


            setScripts(response.data);

              console.log("scripts", scripts);
            } catch (error) {
              console.error('Error:', error);

            }
          };

          fetchData();
        },[])

        const[filteredScripts, setFilteredScripts] = useState(null)
    const[filteredScriptsLength, setFilteredScriptsLength] = useState(null)
    const [counterSign, setCounterSign] =useState(0)

    const handleSearch = (e)=>{
        const searchTerm = e.target.value;
        console.log(searchTerm);
        setCounterSign(e.target.value.length)
        const filteredScripts = scripts.filter((script) =>
        script.name.toLowerCase().includes(searchTerm.toLowerCase())
        );
        setFilteredScriptsLength(filteredScripts.length)
        console.log("filteredScriptsfilteredScripts",filteredScripts);

        onSearch(filteredScripts);
    }

    useEffect(()=>{
        if (filteredScriptsLength === 0) {
            alert('Нет доступных скриптов')
        }

},[filteredScriptsLength])

const handleClickDelete =  () => {
    openModalAllScripts(true)
}

    return (
        <div className={css.Toolbar}>
            <div className={css.items}>
                <div className={css.item_left}>
                    <LinkButton route={''} variant={'btnExit'}>
                       <img src={iconExit} alt="exit" className={css.iconExit}/>
                        <span>Выход</span>
                    </LinkButton>
                    <form action="" className={css.form}>
                        <div className={`${css.form__inner}`}>
                        <label className={counterSign >= 25 ? css.label_error : css.label}>
                            <input type="text" onChange = {(e)=>handleSearch(e)} className={css.search} id="search" placeholder="Поиск по названию" minLength={1} maxLength={25}/>
                            <span className={css.input_counter}>{counterSign}/25</span>
                        </label>
                        <ToastContainer
        position="top-center" // Прямое использование стиля
        autoClose={3000}
      />
                        <Button handleClick={()=> handleClickSearch()} type="submit" variant="searchOperator">
                            <img src={search} alt="search" />
                        </Button>
                        </div>
                    </form>
                </div>
                <div className={css.item_right}>
                        <Button handleClick={handleClickSort} type="button" variant={buttonSort ? 'sortOperator' : 'sortOperatorActive'}>
                          <img src={iconSort} alt="exit" className={css.iconSort}/>
                        </Button>
                    <ReactSelect
                        options={options}
                        placeholder="Сортировать"
                        styles={styles}
                        components={{
                            // IndicatorSeparator: () => null,
                        }}
                        onChange={handleChange}
                    />
                    {pathname !== '/administrator/allscripts' ? <Button handleClick={handleClickView} type="button" variant={buttonView ? 'viewOperator' : 'viewOperatorActive'}>
                          <img src={buttonView ? iconViewCard : iconViewTable} alt="icon-button-view" className={css.iconView}/>
                    </Button> : null}
                    {pathname === '/administrator/allscripts' ? 
                        <Button handleClick={handleClickDelete} type="button" variant={buttonView ? 'viewOperator' : 'viewOperatorActive'}>
                        <img src={basket} alt="basket" className={css.iconView}/>
                  </Button> : null
                    }
                   {pathname !== '/administrator/allscripts' ? <LinkButton
                    route={'dialog'}
                    variant={
                        // valueId !== '' ? 'newDialogActive' : 'newDialog'
                        idScript !== null ? 'newDialogActive' : 'newDialog'
                    }
                    >
                        <img src={iconCross} alt="icon-button-view" className={css.iconCross}/>
                        Новый диалог
                    </LinkButton> : null}
                </div>
            </div>
        </div>
     );
}

export default ToolbarOperatorMain;