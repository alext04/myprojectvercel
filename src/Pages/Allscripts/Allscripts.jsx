import css from "./Allscripts.module.css";

import SideBarMenu from "../../Components/SideBarMenu/SideBarMenu";
import ToolbarOperatorMain from "../../Components/ToolbarOperatorMain/ToolbarOperatorMain";
import TableOperator from "../../Components/TableOperator/TableOperator";

import { useEffect, useState } from "react";
import useAxios from "../../hooks/useAxios";
import { useDispatch, useSelector } from "react-redux"; 
import { scriptId } from "../../store/slices/scriptIdSlice";
import axios from "axios";
import Operator from "../Operator/Operator";
import ModalAllScripts from "../../Components/ModalAllScripts/ModalAllScripts";

function Allscripts() {

    const [scripts, setScripts] = useState([])
    const [openModal, setOpenModal] = useState(false)
    const dispatch = useDispatch()
    const deleteScript = useSelector(state => state.scriptId)

    const fetchData = async () => {
      try {
 let numberScript = 1;
  const response = await axios.get('https://api.doscript.pnpl.tech/api/v2/script/all/');
  setScripts([...response.data].map((item)=>{
                  return {...item, number: numberScript++}
              }).reverse())
      dispatch(scriptId(null))
      } catch (error) {
        console.error('Error:', error);
      }
    };

    useEffect(() => {
          fetchData();
        },[])

        const openModalAllScripts = (data) => {
          setOpenModal(data);
        }
        const closeModalAllScriptsDelete = (data) => {
          axios.delete(`https://api.doscript.pnpl.tech/api/v2/script/del-script/${deleteScript}`)
          .then(res => {
            console.log(res);
            console.log(res.data);
            if(res.status === 200) fetchData()
          })
          // console.log(deleteScript);
          setOpenModal(data);
        }
        const closeModalAllScriptsCancel = (data) => {
          setOpenModal(data)
        }

    return ( <>
            <SideBarMenu />
        <div className={css.container}>
            <ToolbarOperatorMain openModalAllScripts={openModalAllScripts}/>
            <TableOperator scriptsArray={scripts}/>
            
        </div>
        {/* <Operator/> */}
        {openModal ? <ModalAllScripts closeModalAllScriptsDelete={closeModalAllScriptsDelete} closeModalAllScriptsCancel={closeModalAllScriptsCancel}/> : null}
        </> );
}

export default Allscripts;