import { useDispatch, useSelector } from "react-redux";
// import Table from "../../Components/Table/Table";
import Table from "../../Components/Table/Table";
import css from "./Dialog.module.css";
import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import cross from "../../Assets/images/cross-modal.svg";
import { scriptId } from '../../store/slices/scriptIdSlice';

const Dialog = () => {

// let phrase = "5dd899bd-3354-4c5c-9be8-ed812836d60e"
const [operatorPhrase, setOperatorPhrase] = useState([]);
const [clientPhrase, setClientPhrase] = useState([]);
const phrase = useSelector(state => state.scriptId)
  const [nextIdPhrase, setNextIdPhrase] = useState(phrase);
  const [arr, setArr] = useState([]);
  const [activeModal, setActiveModal] = useState(false);
  const [titleBlock, setTitleBlock] = useState()
  const [comments, setComments] = useState("info")
  const [request, setRequest] = useState('')
  const [explanation, setExplanation] = useState('')
  const dispatch = useDispatch()
  console.log(operatorPhrase,clientPhrase);
  async function getPhrase(id) {
    const response = await axios.get(`https://api.doscript.pnpl.tech/api/v2/script/${id}`,
    {
    headers: {
      'Content-Type': 'application/json',
    },
  })
    setOperatorPhrase([...response.data.phrases].map((item)=>{
      return {...item, id: `${id}`}
  }))

    setNextIdPhrase(response.data.phrases[0].uuid)
  }
  useEffect(() => {
    getPhrase(nextIdPhrase)
  },[])
// console.log(operatorPhrase);
  async function getPhraseNext(id) {
    const response = await axios.get(`https://api.doscript.pnpl.tech/api/v2/phrases/${id}`,
    {
    headers: {
      'Content-Type': 'application/json',
    },
  })
  response.data[0].isOperatorPhrase === false ? setClientPhrase([...response.data].map((item)=>{
      return {...item, id: `${id}`}
  })) : setOperatorPhrase([...response.data].map((item)=>{
    return {...item, id: `${id}`}
}))
    setNextIdPhrase(response.data[0].uuid)
  }
  const closeModal = (data) => {
    setActiveModal(data)
  }

  const Modal = ({setActiveModal,titleBlock,openComments,request}) => {
    const [comments, setComments] = useState(openComments)
    const navigate = useNavigate();
    // const [textComment, setTextComment] = useState('')
    const [buttonDisabled, setButtonDisabled] = useState(true)

    const modal = {
      position: "fixed",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      width: "100%",
      height: "100%",
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      background: "rgba(0, 0, 0, 0.5)"
    }
    const wrapper = {
      position: "relative",
      display: "flex",
      width: "694px",
      padding: "64px",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      gap: "32px",
      borderRadius: "10px",
      background: "white"
    }
    const wrapperTitleMain = {
      display: "flex",
      justifyContent: "space-between",
      width: "100%"
    }
    const titleMain = {
      fontFamily: "Lato",
      fontSize: "24px",
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: 1.33,
      alignSelf: "flex-start"
    }
    const modalCloseComment = {
      position: "absolute",
      top: "64px",
      right: "64px",
      cursor: "pointer"
    }
    const modalCloseInfo = {
      position: "absolute",
      top: "30px",
      right: "30px",
      cursor: "pointer"
    }
    const title = {
      marginBottom: "10px",
      fontFamily: "Lato",
      fontSize: "16px",
      fontStyle: "normal",
      fontWeight: "700",
      lineHeight: "24px",
    }
    const subtitle = {
      marginBottom: "30px",
      fontFamily: "Lato",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: "20px",
    }
    const subtitleDialog = {
      fontFamily: "Lato",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "400",
      lineHeight: "20px",
    }
    const buttonWrapper = {
      width: "100%",
      display: "flex",
      justifyContent: "flex-end",
    }
    const buttonLeft = {
      marginLeft: "20px",
      padding: "10px 20px",
      borderRadius: "10px",
      fontFamily: "Roboto",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "20px",
      color: "#156075",
      background: "transparent"
    }
    const buttonRight = {
      marginLeft: "20px",
      padding: "10px 24px",
      borderRadius: "30px",
      fontFamily: "Roboto",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "20px",
      color: "#ffffff",
      background: "rgb(21, 96, 117)",
      opacity: 1,
    }
    const buttonRightDisabled = {
      marginLeft: "20px",
      padding: "10px 24px",
      borderRadius: "30px",
      fontFamily: "Roboto",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "20px",
      color: "#ffffff",
      background: "rgb(21, 96, 117)",
      opacity: 0.5
    }
    const titleWrapper = {
      padding: "24px",
      border: "2px solid #4AB9F5",
      borderRadius: "10px",
      marginBottom: "16px",
    }
    const modalContent = {
      width: "100%",
      height: "100%",
    }
    const contentText = {
      fontFamily: "Lato",
      fontSize: "14px",
      fontWeight: 400,
      lineHeight: 1.42,
      overflowY: "scroll"
    }
    const textareaItem = {
      width: "100%",
      padding: "16px",
      borderRadius: "4px",
      border: "1px solid #D5D5DE",
    }
    const dialog = {
      marginLeft: "20px",
      padding: "10px 20px",
      fontFamily: "Roboto",
      fontSize: "14px",
      fontStyle: "normal",
      fontWeight: "500",
      lineHeight: "20px",
      color: "#156075",
      background: "transparent",
      border: "1px solid #156075",
      borderRadius: "100px",
    }
    const handleClick = () => {

      setActiveModal(false);
      console.log(newComment);
      const comment = {
        text: newComment,
        date: new Date().toLocaleDateString('en-US', { day: 'numeric', month: 'short', year: 'numeric' }),
        time: new Date().toLocaleTimeString('en-US', { hour: 'numeric', minute: 'numeric' })
      };
      const existingComments = JSON.parse(sessionStorage.getItem('comments')) || [];
      const updatedComments = [...existingComments, comment];
      sessionStorage.setItem('comments', JSON.stringify(updatedComments));
      // Получаем значение из sessionStorage
      const storedCommentsString = sessionStorage.getItem('comments');

// Парсим строку JSON
      const storedComments = JSON.parse(storedCommentsString);

// Выводим в консоль
      console.log('storedComments:', storedComments);


      console.log(comment);
      // setComments(true)
      // setComments("info")
      document.querySelector('body').classList.remove('is-lock')

    }
    const handleChange = (e) => {
      const trimmedValue = e.target.value.trim();
      trimmedValue.length >=2 ? (
        setButtonDisabled(false)
      ) : setButtonDisabled(true);
    }

    const handleClickComments = () => {
      // setComments(false)
      setComments("comment")
    }
    const handleClickFinish = () => {
      setActiveModal(false)
      navigate('/operator')
      dispatch(scriptId(null))
    }
    const [newComment, setNewComment] = useState("");

    function saveComment(e){
      /* setComment(e.target.value); */
      setNewComment(e.target.value);
      handleChange(e);
    }

    const showModalContent = (content) => {
      switch(content) {
        case "info":
        return(
             <div className="modal_wrapper" style={wrapper}>
              <img src={cross} alt="cross" className="modal_close" style={modalCloseInfo} onClick={handleClick}/>
            <div className="modal_content"style={modalContent}>
              <h2 className="content_title" style={title}>{titleBlock}</h2>
              <h3 className="content_subtitle" style={subtitle}>{request}</h3>
              <div className="content_text" style={contentText}>{explanation}</div>
            </div>
            {/* <div className="modal_buttons" style={buttonWrapper}>
              <button className="button_comments" style={buttonLeft} onClick={handleClickComments}>Оставить комментарий</button>
              <button className="button_exite" style={buttonRight} onClick={handleClick}>OK</button>
            </div> */}
         </div>);
        case "comment":
        return(
              <div className="modal_wrapper" style={wrapper}>
                {/* <div className="wrapper_title_main" style={wrapperTitleMain}> */}
                  <h2 className="title_main" style={titleMain}>Оставить комментарий</h2>
                  <img src={cross} alt="cross" className="modal_close" style={modalCloseComment} onClick={handleClick}/>
                {/* </div> */}
            <div className="modal_content" style={modalContent}>
              <div className="title_wrapper" style={titleWrapper}>
              <h2 className="content_title" style={title}>{titleBlock}</h2>
              <h3 className="content_subtitle" style={subtitleDialog}>{request}</h3>
              </div>
              <textarea onChange = {(e)=>saveComment(e)}className="content_textarea" name="" id="" cols="30" rows="10" placeholder="%текст_комментария%" style={textareaItem}></textarea>
            </div>
            <div className="modal_buttons" style={buttonWrapper}>
              {/* <button className="button_comments" style={buttonLeft} onClick={handleClick}>Отмена</button> */}
              <button className="button_exite" style={buttonDisabled ? buttonRightDisabled : buttonRight} onClick={handleClick} disabled={buttonDisabled}>Отправить</button>
            </div>
         </div>
        );
        case "finish":
          return(
            <div className="modal_wrapper" style={wrapper}>
         <div className="modal_content"style={modalContent}>
           <div className="content_text">Вы уверены, что хотите завершить диалог?</div>
         </div>
         <div className="modal_buttons" style={buttonWrapper}>
           <button className="button_comments dialog" style={dialog} onClick={handleClickFinish}>Да</button>
           <button className="button_exite" style={buttonRight} onClick={handleClick}>Отменить</button>
         </div>
      </div>
          );

        default: ;
      }
    }
    return(
      <div className="modal" style={modal}>
         {
         showModalContent(comments)
         }
      </div>
    )
  }
  const [clickedElement, setClickedElement] = useState('');

  const handleClick = (e) => {
    e.currentTarget.dataset.blockName !== "Диалог" && getPhraseNext(e.currentTarget.dataset.nextid)
    const nextId = e.currentTarget.dataset.nextid;
    const mergeArrays = [...clientPhrase,...operatorPhrase]
    const clickedElement = mergeArrays.find(item => item.uuid === nextId);
    setArr(arr.some(item => item.uuid === nextId) ? [...arr] : [...arr, clickedElement])
    const temp = e.currentTarget.querySelector('div > p').textContent;



      setClickedElement((prevClickedElement) => {
    //console.log("КЛИК переменная temp", temp);
    //console.log("КЛИК предыдущее значение clickedElement", prevClickedElement);
    return temp;
  });

  }
useEffect(() => {
  //console.log("КЛИК UseEffect", clickedElement);
}, [clickedElement]);


  const handleClickInfo = (e) => {
    e.stopPropagation()
    document.querySelector('body').classList.add('is-lock');
    // console.log(e.currentTarget.dataset.blockName);
    console.log(e.currentTarget.id,e.currentTarget.dataset.blockName);
    if(e.currentTarget.dataset.blockName === "Реплика оператора") {
      operatorPhrase.forEach(item => {
        if(item.id === e.currentTarget.dataset.parentId) {
          setRequest(item.phrase)
          setExplanation(item.note === null ? "Объяснение фразы" : item.note)
          setTitleBlock(e.currentTarget.dataset.blockName)
        }
      })
    } else if(e.currentTarget.dataset.blockName === "Реплика клиента") {
      clientPhrase.forEach(item => {
        if(item.id === e.currentTarget.dataset.parentId) {
          setRequest(item.phrase)
          setExplanation(item.note === null ? "Объяснение фразы" : item.note)
          setTitleBlock(e.currentTarget.dataset.blockName)
        }
      })
    } else if(e.currentTarget.dataset.blockName === "Диалог") {
      arr.forEach(item => {
        if(item.id === e.currentTarget.dataset.parentId) {
          setRequest(item.phrase)
          setExplanation(item.note === null ? "Объяснение фразы" : item.note)
          setTitleBlock(e.currentTarget.dataset.blockName)
        }
      })
    }
    // const titleBlock = e.currentTarget.dataset.blockName;
      // setTitleBlock(titleBlock)
      setActiveModal(true)
      // setComments(true)
      setComments("info")
      //console.log('zzzzz');

  }
  const handleClickCommentsDialog = (e) => {
    e.stopPropagation();
    document.querySelector('body').classList.add('is-lock');
        arr.forEach(item => {
          if(item.id === e.currentTarget.dataset.parentId) {
            setRequest(item.phrase)
            setTitleBlock(e.currentTarget.dataset.blockName)
          }
        })
      // }
      setActiveModal(true)
      // setComments(false)
      setComments("comment")
  }
  const handleClickFinish = () => {
    setActiveModal(true);
    setComments("finish")
  }

  return (
    <>
      <div className={css.wrapper}>
        <div className={css.tool_bar}>
        <div className={css.container_find}>
          <input type="text" placeholder="Поиск" />
          <div className={css.find_icon}>
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
  <path d="M14.9649 14.7549H15.5475L20.0381 19.2545L19.2545 20.0381L14.7549 15.5475V14.9649V14.7631L14.6148 14.6178L14.5335 14.5335L14.6178 14.6148L14.7631 14.7549H14.9649ZM14.0067 14.0067L13.6589 14.3057C12.606 15.2108 11.2406 15.7549 9.75488 15.7549C6.44103 15.7549 3.75488 13.0687 3.75488 9.75488C3.75488 6.44103 6.44103 3.75488 9.75488 3.75488C13.0687 3.75488 15.7549 6.44103 15.7549 9.75488C15.7549 11.2406 15.2108 12.606 14.3057 13.6589L14.0067 14.0067ZM4.75488 9.75488C4.75488 12.521 6.98874 14.7549 9.75488 14.7549C12.521 14.7549 14.7549 12.521 14.7549 9.75488C14.7549 6.98874 12.521 4.75488 9.75488 4.75488C6.98874 4.75488 4.75488 6.98874 4.75488 9.75488Z" fill="#100823" stroke="#111111"/>
</svg>
          </div>
        </div>
        <div className={css.btn_finish_dialog} onClick={handleClickFinish}>Завершить</div>
        </div>
        <div className={css.container}>
          <div className={css.container_replica}>
            <div className={css.replica_client}>
              <h2 className={css.client_title}>Реплики клиента</h2>
              {
                  clientPhrase ? clientPhrase.map((item,index) => {
                    return (
                      <div className={`${css.item_wrapper} ${css.client}`}
                      key={index}
                      onClick={handleClick}
                      // id={idPhrase}
                      id={item.id}
                      data-nextid={item.uuid}
                      data-block-name="Реплика клиента"
                      >
                        <div className={css.item_inner}>
                          <p className={css.item_phrase}>{item.phrase}</p>
                          <p className={css.item_request}>Запрос</p>
                        </div>
                        <div className={css.item_image} onClick={handleClickInfo} data-block-name="Реплика клиента" data-parent-id={item.id}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none" className={css.item_img}>
                            <path d="M7.33301 4.66683H8.66634V6.00016H7.33301V4.66683ZM7.33301 7.3335H8.66634V11.3335H7.33301V7.3335ZM7.99967 1.3335C4.31967 1.3335 1.33301 4.32016 1.33301 8.00016C1.33301 11.6802 4.31967 14.6668 7.99967 14.6668C11.6797 14.6668 14.6663 11.6802 14.6663 8.00016C14.6663 4.32016 11.6797 1.3335 7.99967 1.3335ZM7.99967 13.3335C5.05967 13.3335 2.66634 10.9402 2.66634 8.00016C2.66634 5.06016 5.05967 2.66683 7.99967 2.66683C10.9397 2.66683 13.333 5.06016 13.333 8.00016C13.333 10.9402 10.9397 13.3335 7.99967 13.3335Z" fill="current"/>
                        </svg>
                        </div>
                     </div>
                  )
                  // }
                }) : null
              }
              <div className={css.client_container}></div>
            </div>
            <div className={css.replica_operator}>
            <h2 className={css.operator_title}>Реплики оператора</h2>
              {
                  operatorPhrase ? operatorPhrase.map((item,index) => {
                    return (
                      <div className={item.isAllowed ? `${css.item_wrapper} ${css.operator}` : `${css.item_wrapper} ${css.operator} ${css.notAllowed}`}
                      onClick={handleClick}
                      key={index}
                      // id={idPhrase}
                      id={item.id}
                      data-nextid = {item.uuid}
                      data-block-name="Реплика оператора"
                      >
                        <div className={css.item_inner}>
                        <p className={css.item_phrase}>{item.phrase}</p>
                        <p className={css.item_request}>Запрос</p>
                        </div>
                        <div className={css.item_image} onClick={handleClickInfo} data-block-name="Реплика оператора" data-parent-id={item.id}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none" className={css.item_img}>
                            <path d="M7.33301 4.66683H8.66634V6.00016H7.33301V4.66683ZM7.33301 7.3335H8.66634V11.3335H7.33301V7.3335ZM7.99967 1.3335C4.31967 1.3335 1.33301 4.32016 1.33301 8.00016C1.33301 11.6802 4.31967 14.6668 7.99967 14.6668C11.6797 14.6668 14.6663 11.6802 14.6663 8.00016C14.6663 4.32016 11.6797 1.3335 7.99967 1.3335ZM7.99967 13.3335C5.05967 13.3335 2.66634 10.9402 2.66634 8.00016C2.66634 5.06016 5.05967 2.66683 7.99967 2.66683C10.9397 2.66683 13.333 5.06016 13.333 8.00016C13.333 10.9402 10.9397 13.3335 7.99967 13.3335Z" fill="current"/>
                        </svg>
                        </div>
                    </div>
                  )
                }) : null
              }
            </div>
          </div>
          <div className={css.container_dialog}>
            {
              arr.length > 0 ? arr.map((item,index) => {
                return (
                  <div className={item.isOperatorPhrase ? `${css.item_wrapper} ${css.dialog_operator}` : `${css.item_wrapper} ${css.dialog_client}`}
                  onClick={handleClick}
                  key={index}
                  // id={idPhrase}
                  id={item.id}
                  data-nextid = {item.uuid}
                  data-block-name="Диалог"
                  >
                        <div className={css.item_inner}>
                        <p className={`${css.item_request} ${css.request_dialog}`}>{item.isOperatorPhrase ? "Выявление потребностей" : "Запрос"}</p>
                        <p className={`${css.item_phrase} ${css.phrase_dialog}`}>{item.phrase}</p>
                        </div>
                        <div className={css.item_images}>
                        <div className={css.item_image} onClick={handleClickInfo} data-block-name="Диалог" data-parent-id={item.id}>
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none" className={css.item_img}>
                            <path d="M7.33301 4.66683H8.66634V6.00016H7.33301V4.66683ZM7.33301 7.3335H8.66634V11.3335H7.33301V7.3335ZM7.99967 1.3335C4.31967 1.3335 1.33301 4.32016 1.33301 8.00016C1.33301 11.6802 4.31967 14.6668 7.99967 14.6668C11.6797 14.6668 14.6663 11.6802 14.6663 8.00016C14.6663 4.32016 11.6797 1.3335 7.99967 1.3335ZM7.99967 13.3335C5.05967 13.3335 2.66634 10.9402 2.66634 8.00016C2.66634 5.06016 5.05967 2.66683 7.99967 2.66683C10.9397 2.66683 13.333 5.06016 13.333 8.00016C13.333 10.9402 10.9397 13.3335 7.99967 13.3335Z" fill="current"/>
                          </svg>
                          </div>
                          <div className={css.item_image_comment} onClick={handleClickCommentsDialog} data-block-name="Диалог" data-parent-id={item.id}>
                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none" className={css.item_img}>
                            <path d="M14.6597 2.66683C14.6597 1.9335 14.0663 1.3335 13.333 1.3335H2.66634C1.93301 1.3335 1.33301 1.9335 1.33301 2.66683V10.6668C1.33301 11.4002 1.93301 12.0002 2.66634 12.0002H11.9997L14.6663 14.6668L14.6597 2.66683ZM13.333 2.66683V11.4468L12.553 10.6668H2.66634V2.66683H13.333Z" fill="current"/>
                          </svg>
                          </div>
                        </div>

                    </div>
                )
              }) : null
            }
          </div>
        </div>
        {activeModal ? <Modal setActiveModal={closeModal} titleBlock={titleBlock} openComments={comments} request={request}/> : null}
      </div>
    </>
  );
};

export default Dialog;
