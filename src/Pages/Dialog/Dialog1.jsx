import css from "./Dialog.module.css";
import data from "./script";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import Replica from "../../Components/Replica";
import BlockClient from "../../Components/Replica/BlockClient";
import BlockOperator from "../../Components/Replica/blockOperator";
/* const { script} = data; */

const Dialog = () => {
  const dispatch = useDispatch();
  const replics = useSelector((state) => state.blocks.blocks);


  return (
    <>
      <div className={css.wrapper}>
        <div className={css.tool_bar}>
          <div className={css.container_find}>
            <input type="text" placeholder="Поиск" />
            <div className={css.find_icon}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
              >
                <path
                  d="M14.9649 14.7549H15.5475L20.0381 19.2545L19.2545 20.0381L14.7549 15.5475V14.9649V14.7631L14.6148 14.6178L14.5335 14.5335L14.6178 14.6148L14.7631 14.7549H14.9649ZM14.0067 14.0067L13.6589 14.3057C12.606 15.2108 11.2406 15.7549 9.75488 15.7549C6.44103 15.7549 3.75488 13.0687 3.75488 9.75488C3.75488 6.44103 6.44103 3.75488 9.75488 3.75488C13.0687 3.75488 15.7549 6.44103 15.7549 9.75488C15.7549 11.2406 15.2108 12.606 14.3057 13.6589L14.0067 14.0067ZM4.75488 9.75488C4.75488 12.521 6.98874 14.7549 9.75488 14.7549C12.521 14.7549 14.7549 12.521 14.7549 9.75488C14.7549 6.98874 12.521 4.75488 9.75488 4.75488C6.98874 4.75488 4.75488 6.98874 4.75488 9.75488Z"
                  fill="#100823"
                  stroke="#111111"
                />
              </svg>
            </div>
          </div>
          <div className={css.btn_finish_dialog}>Завершить</div>
        </div>
        <div className={css.container}>
          <div className={css.container_replica}>
            <div className={css.replica_client}>
              <p>Реплики клиента</p>
              <div className={css.client_container}>
                <BlockClient/>
              </div>
            </div>
            <div className={css.replica_operator}>
              <p>Реплики оператора</p>
            </div>
          </div>
          <div className={css.container_dialog}>
            <Replica/>
          </div>
          <div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Dialog;
