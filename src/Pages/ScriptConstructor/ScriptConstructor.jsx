import css from './ScriptConstructor.module.css';
import { useState } from 'react';
import SideBarMenu from '../../Components/SideBarMenu/SideBarMenu';
import Toolbar from '../../Components/Toolbar/Toolbar';
import OperatorWindow from '../../Components/OperatorWindow/OperatorWindow';
import ConstructorWindow from '../../Components/ConstructorWindow/ConstructorWindow';
import { ReactFlowProvider }  from 'reactflow';
import ModalScriptConstructor from '../../Components/ModalScriptConstructor/ModalScriptConstructor';

const ScriptConstructor = () => {
    const [scriptConstructorInner, setScriptConstructorInner] = useState(true);
    const [openModal, setOpenModal] = useState(false)
    const handleClick = (value) => {
        setScriptConstructorInner(value);
    }
    const openModalConstructor = (data) => {
        setOpenModal(data)
    }
    const openModalWindow = () => {
        setOpenModal(false)
    }

    return (
        <>
            <SideBarMenu />
            <Toolbar onClick={handleClick} openModalConstructor={openModalConstructor}/>

            <div className={css.container}>
                {
                    scriptConstructorInner ?
                        <ReactFlowProvider>
                            <ConstructorWindow />
                        </ReactFlowProvider>
                         : 
                        <OperatorWindow />
                }
            </div>
           { openModal ? <ModalScriptConstructor openModal={openModalWindow}/> : null} 
        </>
    )
}

export default ScriptConstructor;