import { useState, useEffect, useCallback } from 'react';
import axios from 'axios'


const instance = axios.create({
    baseURL: `${import.meta.env.VITE_API_HOST_URL}${import.meta.env.VITE_API_PREFIX}`,
    timeout: 1000,
    headers: {
        'Content-Type': 'application/json',
    }
});


const useAxios = (isLoading, url = null, method, token = null, req = null) => {
    const [response, setResponse] = useState(null)
    const [error, setError] = useState(null)

    const fetchData = useCallback(() => {
        const config = {
            method: method,
            url: url,
            data: req
        }

        // instance.interceptors.request.use(
        //     config => {
        //         config.headers['Authorization'] = `Bearer ${token}`

        //         return config
        //     },
        //     error => {
        //         setError(error)
        //         setResponse(null)
        //         return Promise.reject(error)
        //     }
        // )

        instance.request(config)
            .then(response => {
                setResponse(response)
                setError(null)
            })
            .catch(error => {
                setError(error)
                setResponse(null)
            });
    }, [url, method, req])

    useEffect(() => {
        if (url && isLoading) fetchData()
    }, [url, isLoading, req, fetchData])

    return { response, error }
}

export default useAxios