import { createSlice } from '@reduxjs/toolkit';
import { applyEdgeChanges } from 'reactflow';
import { clearState } from './nodesSlice'

// const initialEdges = [{ id: 'e1-2', source: '1', target: '2' }];
const initialState = []

const edgesSlice = createSlice({
    name: 'edges',
    initialState: initialState,

    reducers: {
        updateEdges(state, action) {
            // console.log('payload', action.payload);
            return [...state, action.payload]
        },
        updateEdgesOnChange(state, action) {
            return applyEdgeChanges(action.payload, state)
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(clearState, (state) => {
                return initialState
            })

    },
});

export const { updateEdges, updateEdgesOnChange } = edgesSlice.actions;

export default edgesSlice.reducer;
