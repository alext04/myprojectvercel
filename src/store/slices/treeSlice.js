import { createSlice } from '@reduxjs/toolkit';
import { clearState } from './nodesSlice';
import { v4 as uuidv4 } from 'uuid';

// const initialTree = [
//     { id: '1', data: { label: '1' }, dChildren: ['2'] },
//     { id: '2', data: { label: '2' }, dChildren: [] },
// ]

const initialState = {
    script: {
        scriptId: '',
        scriptName: '',
        isActive: false
    },
    nodes: [

    ]
}

const treeSlice = createSlice({
    name: 'tree',
    initialState: initialState,

    reducers: {
        updateScriptId(state, action) {

        },
        updateScriptName(state, action) {

        },
        updateScriptIdName(state, action) {
            return {
                ...state,
                script: {
                    ...state.script,
                    scriptId: action.payload.scriptId,
                    scriptName: action.payload.scriptName
                }
            }
        },
        toggleActiveState(state, action) {
            return {
                ...state,
                script: {...state.script, isActive: !state.script.isActive}
            }
        },
        updateTree(state, action) {
            // console.log(action.payload)
            return {
                ...state,
                nodes: action.payload
            }
        },
        addTreeNode(state, action) {
            // console.log(action.payload)
            return {
                ...state,
                nodes: [...state.nodes, action.payload] 
            }
        },
        removeTreeNode(state, action) {
            // return {
            //     ...state,
            //     nodes: state.nodes.map(item => item.uuid === action.payload ? {...item, status: "cool",isRoot: false} : item)
            // }
            return {
                ...state,
                nodes: state.nodes.filter(item => item.uuid !== action.payload)
            }
        },
        updateTreeNodeData(state, action) {
            // console.log('payload', action.payload.data.label)
            return {
                ...state,
                nodes: state.nodes.map(item => {
                    if (item.uuid === action.payload.uuid) {
                        item = {
                            ...item,
                            data: {
                                ...item.data,
                                label: action.payload.data.label,
                                // content: action.payload.data.content
                                note: action.payload.data.note
                            }
                        }
                    }
                    return item
                }) 
            }
        },
        updateSpecificTreeNode(state, action) {
            return {
                ...state,
                nodes: state.nodes.map(item => {
                    if (item.uuid === action.payload.uuid) {
                        item = action.payload
                    }
                    return item
                }) 
            }
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(clearState, (state) => {
                return initialState
            })

    },
});

export const {
    updateTree,
    addTreeNode,
    removeTreeNode,
    toggleActiveState,
    updateScriptIdName,
    updateScriptName,
    updateScriptId,
    updateTreeNodeData,
    updateSpecificTreeNode
} = treeSlice.actions;

export default treeSlice.reducer;